#!/bin/sh

goose -dir migrations/master postgres "host=postgres user=postgres password=password dbname=orders sslmode=disable" up
goose -dir migrations/shard1 postgres "host=postgres_shard1 user=postgres password=password dbname=orders sslmode=disable" up
goose -dir migrations/shard2 postgres "host=postgres_shard2 user=postgres password=password dbname=orders sslmode=disable" up
