protoc --go_out=../pkg/api --go-grpc_out=../pkg/api --grpc-gateway_out=../pkg/api --openapiv2_out=../api ^
    --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --grpc-gateway_opt=paths=source_relative ^
    --grpc-gateway_opt=generate_unbound_methods=true ^
    --proto_path=../proto ../proto/orders/api.proto