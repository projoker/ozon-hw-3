package configs

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

type Order struct {
	DsnDB    string   `yaml:"dsnDB"`
	DsnCache string   `yaml:"dsnCache"`
	Nodes    []string `yaml:"nodes"`
}

type Monitor struct {
	Nodes []string `yaml:"nodes"`
}

type Retries struct {
	Order uint64 `yaml:"order"`
	Sale  uint64 `yaml:"sale"`
	Time  uint64 `yaml:"time"`
}

type Notifications struct {
	DsnDB   string   `yaml:"dsnDB"`
	Retries Retries  `yaml:"retries"`
	Nodes   []string `yaml:"nodes"`
}

func ParseConfig(path string, config interface{}) error {
	confFile, err := os.ReadFile(path)
	if err != nil {
		log.Fatalf("failed to read configs: %v", err)
	}

	err = yaml.Unmarshal(confFile, config)

	return err
}
