package api

import (
	"errors"
)

var ErrorCartNotFound = errors.New("cart for user not found")
var ErrorProductNotFound = errors.New("product in cart not found")
var ErrorOrderNotFound = errors.New("order for user not found")
var ErrorNotificationSend = errors.New("notification send error")
