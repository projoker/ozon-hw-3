-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS cart_products1
(
    user_id    INT NOT NULL,
    product_id INT NOT NULL,
    count      INT NOT NULL CHECK (count > 0),
    UNIQUE (user_id, product_id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE cart_products1 CASCADE;
-- +goose StatementEnd
