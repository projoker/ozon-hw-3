-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS cart_products
(
    user_id    INT NOT NULL,
    product_id INT NOT NULL,
    count      INT NOT NULL CHECK (count > 0)
)
    PARTITION BY HASH (user_id);

CREATE EXTENSION postgres_fdw;
CREATE SERVER shard1 FOREIGN DATA WRAPPER postgres_fdw options (host 'postgres_shard1', port '5432', dbname 'orders');
CREATE SERVER shard2 FOREIGN DATA WRAPPER postgres_fdw options (host 'postgres_shard2', port '5432', dbname 'orders');
CREATE USER MAPPING FOR postgres SERVER shard1 OPTIONS (user 'postgres', password 'password');
CREATE USER MAPPING FOR postgres SERVER shard2 OPTIONS (user 'postgres', password 'password');

CREATE FOREIGN TABLE cart_products1 PARTITION OF cart_products
    FOR VALUES WITH (modulus 2, remainder 0) server shard1;
CREATE FOREIGN TABLE cart_products2 PARTITION OF cart_products
    FOR VALUES WITH (modulus 2, remainder 1) server shard2;

CREATE TYPE STATUS AS ENUM ('ok', 'reset');
CREATE TABLE IF NOT EXISTS orders
(
    id      SERIAL PRIMARY KEY,
    user_id INT NOT NULL,
    status  STATUS DEFAULT 'ok'
);

CREATE TABLE IF NOT EXISTS orders_products
(
    order_id INT REFERENCES orders (id),
    id       INT NOT NULL,
    count    INT NOT NULL CHECK (count > 0),
    PRIMARY KEY (order_id, id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE cart_products CASCADE;
DROP USER MAPPING FOR postgres SERVER shard1;
DROP USER MAPPING FOR postgres SERVER shard2;
DROP SERVER shard1;
DROP SERVER shard2;
DROP EXTENSION postgres_fdw;
DROP TABLE orders CASCADE;
DROP TABLE orders_products CASCADE;
-- +goose StatementEnd
