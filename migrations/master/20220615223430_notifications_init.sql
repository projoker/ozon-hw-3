-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS notifications
(
    id      SERIAL PRIMARY KEY,
    user_id INT  NOT NULL,
    text    TEXT NOT NULL,
    retries INT  NOT NULL,
    time    TIMESTAMP DEFAULT now()
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE notifications CASCADE;
-- +goose StatementEnd
