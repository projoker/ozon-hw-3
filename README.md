# Микросервисы Ozon

## Схема

Схема всего Ozon и детализация 3 микросервисов: https://miro.com/app/board/uXjVOvobv10=

## Микросервисы

Реализованы 3 микросервиса, связанные одной транзакцией:

* Сервис заказов
* Сервис мониторинга
* Сервис уведомлений

### Установка

Запуск kafka в docker-compose: `docker-compose -f deployments/kafka/docker-compose.yml up -d`

Запуск сервисов в docker-compose: `docker-compose -f deployments/ozon/docker-compose.yml up -d`

### Взаимодействие

Внешние запросы идут в сервис заказа. Отправлять можно через Postman, загрузив `proto/orders/api.proto`
или `api/orders/api.swagger.json`

* `GRPC localhost:5300` - обработчики gRPC
* http://localhost:80 - REST прокси для gRPC (документация OpenApi)

### Observability

* http://localhost:8080 - Kafka UI
* http://localhost:9090 - Prometheus

### Saga

1. **Сервис заказов** - компенсируемая транзакция.<br>
   Получает изменение/завершение формирования заказа по api, отправляет завершенные заказы в _created_orders_. Читает отказы из _reser_orders_.
2. **Сервис мониторинга** - поворотная транзакция.<br>
   Читает заказы из _created_orders_, отправляет изменение статуса в _status_ и отказы в _reset_orders_.
3. **Сервис уведомлений** - повторяемая транзакция.<br>
   Читает _status_, при ошибке повторяет попытку позже

### Топики Apache Kafka

* **created_orders** - созданные заказы
* **status** - изменение статуса
* **reset_orders** - отказы

### Хранение данных

* **PostgreSQL** - шардированные данные корзин и заказов
* **Memcached** - кеш корзины