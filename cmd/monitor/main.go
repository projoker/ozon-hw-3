package main

import (
	"context"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/configs"
	"gitlab.ozon.dev/Projoker/homework-3/internal/metrics"
	"gitlab.ozon.dev/Projoker/homework-3/internal/monitor"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	conf := configs.Monitor{}
	err := configs.ParseConfig("./configs/monitor.yaml", &conf)
	if err != nil {
		log.Fatalf("parse config error: %v", err)
	}

	confSarama := sarama.NewConfig()
	confSarama.Consumer.Offsets.Initial = sarama.OffsetOldest

	metrics.Start("monitor", ":2112", confSarama.MetricRegistry)

	consumerGroup, err := sarama.NewConsumerGroup(conf.Nodes, "monitor", confSarama)
	if err != nil {
		log.Fatal(err)
	}
	producer, err := sarama.NewAsyncProducer(conf.Nodes, confSarama)
	if err != nil {
		log.Fatal(err)
	}

	server := monitor.New(consumerGroup, producer)
	server.RunProducer()
	server.RunConsumerGroup(ctx)

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	select {
	case <-exit:
		cancel()
		log.Println("graceful shutdown...")
	case <-ctx.Done():
		log.Println("graceful shutdown on context...")
	}

	server.Wg.Wait()

	err = producer.Close()
	if err != nil {
		log.Fatalf("error closing producer: %v", err)
	}
	err = consumerGroup.Close()
	if err != nil {
		log.Fatalf("error closing consumerGroup: %v", err)
	}
}
