package main

import (
	"context"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/configs"
	"gitlab.ozon.dev/Projoker/homework-3/internal/db"
	"gitlab.ozon.dev/Projoker/homework-3/internal/metrics"
	"gitlab.ozon.dev/Projoker/homework-3/internal/notifications"
	"gitlab.ozon.dev/Projoker/homework-3/internal/repository"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	conf := configs.Notifications{}
	err := configs.ParseConfig("./configs/notifications.yaml", &conf)
	if err != nil {
		log.Fatalf("parse config error: %v", err)
	}

	confSarama := sarama.NewConfig()
	confSarama.Consumer.Offsets.Initial = sarama.OffsetOldest

	metrics.Start("notifications", ":2112", confSarama.MetricRegistry)

	adp, err := db.New(context.Background(), conf.DsnDB)
	if err != nil {
		log.Fatalf("failed to connect DB: %v", err)
	}

	consumerGroup, err := sarama.NewConsumerGroup(conf.Nodes, "notifications", confSarama)
	if err != nil {
		log.Fatal(err)
	}

	server := notifications.New(consumerGroup, repository.New(adp), conf.Retries)
	server.RunConsumerGroup(ctx)
	server.RunRetryer(ctx)
	server.RunSaler(ctx)

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	select {
	case <-exit:
		cancel()
		log.Println("graceful shutdown...")
	case <-ctx.Done():
		log.Println("graceful shutdown on context...")
	}

	server.Wg.Wait()

	err = consumerGroup.Close()
	if err != nil {
		log.Fatalf("error closing consumerGroup: %v", err)
	}
}
