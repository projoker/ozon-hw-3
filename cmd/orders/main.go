package main

import (
	"context"
	"github.com/Shopify/sarama"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.ozon.dev/Projoker/homework-3/configs"
	"gitlab.ozon.dev/Projoker/homework-3/internal/cache"
	"gitlab.ozon.dev/Projoker/homework-3/internal/db"
	"gitlab.ozon.dev/Projoker/homework-3/internal/metrics"
	"gitlab.ozon.dev/Projoker/homework-3/internal/orders"
	"gitlab.ozon.dev/Projoker/homework-3/internal/repository"
	"gitlab.ozon.dev/Projoker/homework-3/pkg/api/orders"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func runGRPCWithProxy(ctx context.Context, server *orders.Server) {
	lis, err := net.Listen("tcp", ":5300")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	api.RegisterCartServiceServer(grpcServer, server)
	go func() {
		err = grpcServer.Serve(lis)
		if err != nil {
			log.Fatalf("failed to start grpc: %v", err)
		}
	}()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err = api.RegisterCartServiceHandlerFromEndpoint(ctx, mux, "localhost:5300", opts)
	if err != nil {
		log.Fatalf("failed to start proxy: %v", err)
	}
	go func() {
		err = http.ListenAndServe(":80", mux)
		if err != nil {
			log.Fatalf("failed to listen proxy: %v", err)
		}
	}()
}

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	conf := configs.Order{}
	err := configs.ParseConfig("./configs/orders.yaml", &conf)
	if err != nil {
		log.Fatalf("parse config error: %v", err)
	}

	confSarama := sarama.NewConfig()
	confSarama.Consumer.Offsets.Initial = sarama.OffsetOldest

	metrics.Start("orders", ":2112", confSarama.MetricRegistry)

	adp, err := db.New(context.Background(), conf.DsnDB)
	if err != nil {
		log.Fatalf("failed to connect DB: %v", err)
	}

	consumerGroup, err := sarama.NewConsumerGroup(conf.Nodes, "orders", confSarama)
	if err != nil {
		log.Fatal(err)
	}
	producer, err := sarama.NewAsyncProducer(conf.Nodes, confSarama)
	if err != nil {
		log.Fatal(err)
	}

	server := orders.New(consumerGroup, producer, repository.New(adp), cache.New(conf.DsnCache))
	server.RunProducer()
	server.RunConsumerGroup(ctx)

	go runGRPCWithProxy(ctx, server)

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	select {
	case <-exit:
		cancel()
		log.Println("graceful shutdown...")
	case <-ctx.Done():
		log.Println("graceful shutdown on context...")
	}

	server.Wg.Wait()

	err = server.Producer.Close()
	if err != nil {
		log.Fatalf("error closing producer: %v", err)
	}
	err = server.ConsumerGroup.Close()
	if err != nil {
		log.Fatalf("error closing consumerGroup: %v", err)
	}
}
