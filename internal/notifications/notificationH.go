package notifications

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/Projoker/homework-3/pkg/api/orders"
	"log"
	"math/rand"
)

func (server *Server) Sale(ctx context.Context, id int, retries uint64) error {
	return server.notify(ctx, 0, fmt.Sprintf("Sale #%v started!", id), retries)
}

func (server *Server) orderStatus(ctx context.Context, userId, orderId uint64, value string, retries uint64) error {
	return server.notify(ctx, userId, fmt.Sprintf("Status of %v become %s", orderId, value), retries)
}

func (server *Server) notify(ctx context.Context, userId uint64, text string, retries uint64) (err error) {
	if rand.Intn(4) == 1 {
		if retries > 0 {
			err = server.r.AddNotification(ctx, userId, text, retries-1)
		}

		return api.ErrorNotificationSend
	}

	log.Printf("notification send for %v: %v", userId, text)

	return
}
