package notifications

import (
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/configs"
	"log"
	"sync"
	"time"
)

type Server struct {
	ConsumerGroup sarama.ConsumerGroup
	r             Repository
	Wg            sync.WaitGroup
	retries       configs.Retries
}

func New(consumerGroup sarama.ConsumerGroup, r Repository, retries configs.Retries) *Server {
	return &Server{ConsumerGroup: consumerGroup, r: r, retries: retries}
}

func (server *Server) RunConsumerGroup(ctx context.Context) {
	go func() {
		for err := range server.ConsumerGroup.Errors() {
			log.Printf("consumer error: %v", err)
		}
	}()

	server.Wg.Add(1)
	go func() {
		defer server.Wg.Done()
		for {
			err := server.ConsumerGroup.Consume(ctx, []string{"status"}, server)
			if err != nil {
				log.Printf("consume error: %v", err)
			}
			if ctx.Err() != nil {
				return
			}
		}
	}()
}

func (server *Server) RunSaler(ctx context.Context) {
	go func() {
		// fake sale
		for i := 0; ; i++ {
			err := server.Sale(ctx, i, server.retries.Sale)
			if err != nil {
				log.Println("cant send sale", err)
			}

			time.Sleep(time.Minute * 5)
		}
	}()
}

func (server *Server) RunRetryer(ctx context.Context) {
	go func() {
		for {
			time.Sleep(time.Minute)
			log.Println("checking retries")

			notifications, err := server.r.GetNotifications(ctx, server.retries.Time)
			if err != nil {
				log.Println(fmt.Sprintf("cant get notifications: %v", err))
				continue
			}

			for _, notification := range notifications {
				err = server.r.DeleteNotification(ctx, notification.Id)
				if err != nil {
					log.Println(fmt.Sprintf("cant delete retry %v", notification.Id))
					continue
				}

				err = server.notify(ctx, notification.UserId, notification.Text, notification.Retries)
				if err != nil {
					log.Println(fmt.Sprintf("cant send notification to %v, retires: %v, err: %v", notification.UserId, notification.Retries, err))
				}
			}
		}
	}()
}
