package notifications

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"log"
	"strconv"
)

func (server *Server) Setup(session sarama.ConsumerGroupSession) error {
	return nil
}

func (server *Server) Cleanup(session sarama.ConsumerGroupSession) error {
	log.Println("consumption stopped")
	return nil
}

func (server *Server) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	log.Println("consume partition")
	for {
		select {
		case msg := <-claim.Messages():
			log.Printf("recieved status for %s order: %s", msg.Key, msg.Value)

			userId, err := strconv.ParseUint(string(msg.Key), 10, 64)
			if err != nil {
				log.Println(err)
			}

			orderStatus := models.OrderStatus{}
			err = json.Unmarshal(msg.Value, &orderStatus)
			if err != nil {
				log.Println(err)
			}

			err = server.orderStatus(session.Context(), userId, orderStatus.Id, orderStatus.Status, server.retries.Order)
			if err != nil {
				log.Println(fmt.Sprintf("cant send status to %v: %v", userId, err))
			}

			session.MarkMessage(msg, "")
		case <-session.Context().Done():
			return nil
		}
	}
}
