package notifications

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
)

type Repository interface {
	AddNotification(ctx context.Context, userId uint64, text string, retries uint64) (err error)
	DeleteNotification(ctx context.Context, id uint64) (err error)
	GetNotifications(ctx context.Context, interval uint64) (notifications []models.Notification, err error)
}
