package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rcrowley/go-metrics"
	"log"
	"net/http"
	"time"
)

var (
	startTime = time.Now().UTC()
)

func Start(name string, addr string, saramaRegistry metrics.Registry) {
	log.Println("starting metrics")
	setup(name, saramaRegistry)

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		err := http.ListenAndServe(addr, nil)
		if err != nil {
			log.Fatalf("http listen error: %v", err)
		}
	}()
}

func setup(name string, saramaRegistry metrics.Registry) {
	prometheus.MustRegister(prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   "ozon",
			Name:        "uptime",
			Help:        "Number of seconds from start.",
			ConstLabels: prometheus.Labels{"service": name},
		},
		func() float64 {
			return time.Since(startTime).Seconds()
		},
	))

	prometheus.MustRegister(prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   "ozon",
			Name:        "request_rate",
			Help:        "Number of working goroutines.",
			ConstLabels: prometheus.Labels{"service": name},
		},
		func() float64 {
			return metrics.GetOrRegisterMeter("request-rate", saramaRegistry).RateMean()
		},
	))

	prometheus.MustRegister(prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   "ozon",
			Name:        "response_rate",
			Help:        "Rate of kafka goroutines.",
			ConstLabels: prometheus.Labels{"service": name},
		},
		func() float64 {
			return metrics.GetOrRegisterMeter("response-rate", saramaRegistry).RateMean()
		},
	))

	prometheus.MustRegister(prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   "ozon",
			Name:        "requests_in_flight",
			Help:        "Number of kafka requests in flight.",
			ConstLabels: prometheus.Labels{"service": name},
		},
		func() float64 {
			return float64(metrics.GetOrRegisterCounter("requests-in-flight", saramaRegistry).Count())
		},
	))
}
