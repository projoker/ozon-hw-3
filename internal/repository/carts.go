package repository

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"gitlab.ozon.dev/Projoker/homework-3/pkg/api/orders"
)

func (r *repository) AddProduct(ctx context.Context, userId, id uint64) (err error) {
	const query = `
		INSERT INTO cart_products
		VALUES ($1, $2, 1)
	`

	_, err = r.pool.Exec(ctx, query, userId, id)

	return
}

func (r *repository) UpdateProduct(ctx context.Context, userId, id uint64, count uint32) (err error) {
	const query = `
		UPDATE cart_products
		SET count = $1
		WHERE user_id = $2
		  AND product_id = $3
	`

	cmd, err := r.pool.Exec(ctx, query, count, userId, id)

	if cmd.RowsAffected() == 0 {
		err = api.ErrorProductNotFound
		return
	}

	return
}

func (r *repository) DeleteProduct(ctx context.Context, userId, id uint64) (err error) {
	const query = `
		DELETE
		FROM cart_products
		WHERE user_id = $1
		  AND product_id = $2
	`

	cmd, err := r.pool.Exec(ctx, query, userId, id)

	if cmd.RowsAffected() == 0 {
		err = api.ErrorProductNotFound
		return
	}

	return
}

func (r *repository) GetCart(ctx context.Context, userId uint64) (products []models.Product, err error) {
	const query = `
		SELECT product_id, count
		FROM cart_products
		WHERE user_id = $1
	`

	rows, err := r.pool.Query(ctx, query, userId)
	if err != nil {
		return
	}
	defer rows.Close()

	products = make([]models.Product, 0)

	for rows.Next() {
		var product models.Product
		err = rows.Scan(&product.Id, &product.Count)
		if err != nil {
			return
		}
		products = append(products, product)
	}

	return
}

func (r *repository) DeleteCart(ctx context.Context, userId uint64) (err error) {
	const query = `
		DELETE
		FROM cart_products
		WHERE user_id = $1
	`

	_, err = r.pool.Exec(ctx, query, userId)

	return
}
