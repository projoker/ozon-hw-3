package repository

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"time"
)

func (r *repository) AddNotification(ctx context.Context, userId uint64, text string, retries uint64) (err error) {
	const query = `
		INSERT INTO notifications (user_id, text, retries)
		VALUES ($1, $2, $3)
	`

	_, err = r.pool.Exec(ctx, query, userId, text, retries)

	return
}

func (r *repository) DeleteNotification(ctx context.Context, id uint64) (err error) {
	const query = `
		DELETE
		FROM notifications
		WHERE id = $1
	`

	_, err = r.pool.Exec(ctx, query, id)

	return
}

func (r *repository) GetNotifications(ctx context.Context, interval uint64) (notifications []models.Notification, err error) {
	const query = `
		SELECT id, user_id, text, retries
		FROM notifications
		WHERE time > now() - $1::INTERVAL
	`

	rows, err := r.pool.Query(ctx, query, time.Duration(interval)*time.Second)
	if err != nil {
		return
	}
	defer rows.Close()

	notifications = make([]models.Notification, 0)

	for rows.Next() {
		var notification models.Notification
		err = rows.Scan(&notification.Id, &notification.UserId, &notification.Text, &notification.Retries)
		if err != nil {
			return
		}
		notifications = append(notifications, notification)
	}

	return
}
