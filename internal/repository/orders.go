package repository

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"gitlab.ozon.dev/Projoker/homework-3/pkg/api/orders"
)

func (r *repository) CreateOrder(ctx context.Context, userId uint64, products []models.Product) (id uint64, err error) {
	tx, err := r.pool.Begin(ctx)
	if err != nil {
		return
	}
	defer tx.Rollback(ctx)

	const query = `
		INSERT INTO orders (user_id)
		VALUES ($1)
		RETURNING id
	`
	err = tx.QueryRow(ctx, query, userId).Scan(&id)
	if err != nil {
		return
	}

	const queryProduct = `
		INSERT INTO orders_products
		VALUES ($1, $2, $3)
	`
	for _, product := range products {
		_, err = tx.Exec(ctx, queryProduct, id, product.Id, product.Count)
		if err != nil {
			return
		}
	}

	err = tx.Commit(ctx)

	return
}

func (r *repository) UpdateStatus(ctx context.Context, id uint64, status string) (err error) {
	const query = `
		UPDATE orders
		SET status = $2
		WHERE id = $1
	`

	cmd, err := r.pool.Exec(ctx, query, id, status)

	if cmd.RowsAffected() == 0 {
		err = api.ErrorOrderNotFound
		return
	}

	return
}
