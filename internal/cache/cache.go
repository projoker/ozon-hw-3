package cache

import (
	"context"
	"encoding/json"
	"github.com/bradfitz/gomemcache/memcache"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"strconv"
)

type cache struct {
	client *memcache.Client
}

func New(dsn string) *cache {
	mc := memcache.New(dsn)

	return &cache{mc}
}

func (c *cache) SetCart(ctx context.Context, userId uint64, products []models.Product) (err error) {
	data, err := json.Marshal(products)
	if err != nil {
		return
	}

	err = c.client.Set(&memcache.Item{
		Key:        strconv.FormatUint(userId, 10),
		Value:      data,
		Expiration: 86400,
	})

	return
}

func (c *cache) GetCart(ctx context.Context, userId uint64) (products []models.Product, err error) {
	item, err := c.client.Get(strconv.FormatUint(userId, 10))
	if err != nil {
		return
	}

	err = json.Unmarshal(item.Value, &products)

	return
}

func (c *cache) DeleteCart(ctx context.Context, userId uint64) (err error) {

	err = c.client.Delete(strconv.FormatUint(userId, 10))

	return
}
