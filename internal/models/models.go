package models

type Order struct {
	Id       uint64
	Products []Product
}

type Product struct {
	Id    uint64
	Count int32
}

type OrderStatus struct {
	Id     uint64
	Status string
}

type Notification struct {
	Id      uint64
	UserId  uint64
	Text    string
	Retries uint64
}
