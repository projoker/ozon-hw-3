package monitor

import (
	"encoding/json"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"log"
	"math/rand"
	"strconv"
)

func (server *Server) Setup(session sarama.ConsumerGroupSession) error {
	return nil
}

func (server *Server) Cleanup(session sarama.ConsumerGroupSession) error {
	log.Println("consumption stopped")
	return nil
}

func (server *Server) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	log.Println("consume partition")
	for {
		select {
		case msg := <-claim.Messages():
			log.Printf("recieved order %s: %s", msg.Key, msg.Value)

			userId, err := strconv.ParseUint(string(msg.Key), 10, 64)
			if err != nil {
				log.Println(err)
			}

			order := models.Order{}
			err = json.Unmarshal(msg.Value, &order)
			if err != nil {
				log.Println(err)
			}

			// fake order reset or deliver
			if rand.Intn(5) == 1 {
				server.Reset(order.Id)
			} else {
				server.UpdateStatus(userId, order.Id, "delivered")
			}

			session.MarkMessage(msg, "")
		case <-session.Context().Done():
			return nil
		}
	}
}
