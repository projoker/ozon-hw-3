package monitor

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"log"
	"strconv"
)

func (server *Server) UpdateStatus(userId uint64, orderId uint64, status string) {
	log.Printf("update order status: %v", orderId)

	statusJson, err := json.Marshal(
		models.OrderStatus{
			Id:     orderId,
			Status: status,
		},
	)
	if err != nil {
		return
	}

	server.Producer.Input() <- &sarama.ProducerMessage{
		Topic: "status",
		Key:   sarama.StringEncoder(strconv.FormatUint(userId, 10)),
		Value: sarama.StringEncoder(statusJson),
	}
}

func (server *Server) Reset(orderId uint64) {
	log.Printf("reset order: %v", orderId)

	server.Producer.Input() <- &sarama.ProducerMessage{
		Topic: "reset_orders",
		Key:   sarama.StringEncoder(fmt.Sprintf("%v", orderId)),
		Value: sarama.StringEncoder("reset"),
	}
}
