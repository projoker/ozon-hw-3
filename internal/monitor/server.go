package monitor

import (
	"context"
	"github.com/Shopify/sarama"
	"log"
	"sync"
)

type Server struct {
	ConsumerGroup sarama.ConsumerGroup
	Producer      sarama.AsyncProducer
	Wg            sync.WaitGroup
}

func New(consumerGroup sarama.ConsumerGroup, producer sarama.AsyncProducer) *Server {
	return &Server{ConsumerGroup: consumerGroup, Producer: producer}
}

func (server *Server) RunConsumerGroup(ctx context.Context) {
	go func() {
		for err := range server.ConsumerGroup.Errors() {
			log.Printf("consumer error: %v", err)
		}
	}()

	server.Wg.Add(1)
	go func() {
		defer server.Wg.Done()
		for {
			err := server.ConsumerGroup.Consume(ctx, []string{"created_orders"}, server)
			if err != nil {
				log.Printf("consume error: %v", err)
			}
			if ctx.Err() != nil {
				return
			}
		}
	}()
}

func (server *Server) RunProducer() {
	go func() {
		for err := range server.Producer.Errors() {
			log.Printf("producer error: %v", err)
		}
	}()
}
