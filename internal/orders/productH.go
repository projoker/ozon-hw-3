package orders

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-3/pkg/api/orders"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
)

func (server *Server) AddProduct(ctx context.Context, req *api.AddProductReq) (resp *emptypb.Empty, err error) {
	log.Printf("add product for %v: %v", req.UserId, req.ProductId)
	resp = &emptypb.Empty{}

	err = server.r.AddProduct(ctx, req.UserId, req.ProductId)
	if err != nil {
		log.Printf("error add product: %v", err)
	}

	_ = server.c.DeleteCart(ctx, req.UserId)

	return
}

func (server *Server) UpdateProduct(ctx context.Context, req *api.UpdateProductReq) (resp *emptypb.Empty, err error) {
	log.Printf("update product for %v: %v x%v", req.UserId, req.ProductId, req.Count)
	resp = &emptypb.Empty{}

	if req.Count == 0 {
		err = server.r.DeleteProduct(ctx, req.UserId, req.ProductId)
	} else {
		err = server.r.UpdateProduct(ctx, req.UserId, req.ProductId, req.Count)
	}

	if err != nil {
		log.Printf("error update product: %v", err)
		return
	}

	_ = server.c.DeleteCart(ctx, req.UserId)

	return
}
