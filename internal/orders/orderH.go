package orders

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
	"gitlab.ozon.dev/Projoker/homework-3/pkg/api/orders"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
)

func (server *Server) GetCart(ctx context.Context, req *api.GetCartReq) (resp *api.GetCartResp, err error) {
	log.Printf("get cart: %v", req.UserId)
	resp = &api.GetCartResp{Products: make([]*api.Product, 0)}

	products, err := server.c.GetCart(ctx, req.UserId)
	if err != nil {
		products, err = server.r.GetCart(ctx, req.UserId)
		if err != nil {
			log.Printf("error get cart: %v", err)
			return
		}
		err = server.c.SetCart(ctx, req.UserId, products)
		if err != nil {
			log.Printf("error cache set cart: %v", err)
		}
	}

	for _, product := range products {
		resp.Products = append(resp.Products, &api.Product{Id: product.Id, Count: product.Count})
	}

	return
}

func (server *Server) FinishCart(ctx context.Context, req *api.FinishCartReq) (resp *emptypb.Empty, err error) {
	log.Printf("finish cart: %v", req.UserId)
	resp = &emptypb.Empty{}

	products, err := server.c.GetCart(ctx, req.UserId)
	if err != nil {
		products, err = server.r.GetCart(ctx, req.UserId)
		if err != nil {
			log.Printf("error get cart: %v", err)
			return
		}
	}

	if len(products) == 0 {
		err = api.ErrorCartNotFound
		return
	}

	id, err := server.r.CreateOrder(ctx, req.UserId, products)
	if err != nil {
		log.Printf("error create order: %v", err)
		return
	}

	orderJson, err := json.Marshal(models.Order{Id: id, Products: products})
	if err != nil {
		return
	}

	err = server.r.DeleteCart(ctx, req.UserId)
	if err != nil {
		log.Printf("error delete cart: %v", err)
		return
	}
	_ = server.c.DeleteCart(ctx, req.UserId)
	_ = server.c.DeleteCart(ctx, req.UserId)

	server.Producer.Input() <- &sarama.ProducerMessage{
		Topic: "created_orders",
		Key:   sarama.StringEncoder(fmt.Sprintf("%v", req.UserId)),
		Value: sarama.StringEncoder(orderJson),
	}

	return
}

func (server *Server) RollbackOrder(ctx context.Context, orderId uint64, status string) {
	log.Printf("rollback reset order: %v", orderId)

	err := server.r.UpdateStatus(ctx, orderId, status)
	if err != nil {
		log.Printf("error update status: %v", err)
	}
}
