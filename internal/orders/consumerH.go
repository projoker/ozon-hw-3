package orders

import (
	"github.com/Shopify/sarama"
	"log"
	"strconv"
)

func (server *Server) Setup(session sarama.ConsumerGroupSession) error {
	return nil
}

func (server *Server) Cleanup(session sarama.ConsumerGroupSession) error {
	log.Println("consumption stopped")
	return nil
}

func (server *Server) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	log.Println("consume partition")
	for {
		select {
		case msg := <-claim.Messages():
			log.Printf("recieved reset order %s, status %s", msg.Key, msg.Value)

			id, err := strconv.ParseUint(string(msg.Key), 10, 64)
			if err != nil {
				log.Println(err)
			}
			server.RollbackOrder(session.Context(), id, string(msg.Value))

			session.MarkMessage(msg, "")
		case <-session.Context().Done():
			return nil
		}
	}
}
