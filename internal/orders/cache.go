package orders

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
)

type Cache interface {
	SetCart(ctx context.Context, userId uint64, products []models.Product) (err error)
	GetCart(ctx context.Context, userId uint64) (products []models.Product, err error)
	DeleteCart(ctx context.Context, userId uint64) error
}
