package orders

import (
	"context"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/Projoker/homework-3/pkg/api/orders"
	"log"
	"sync"
)

type Server struct {
	api.UnimplementedCartServiceServer
	ConsumerGroup sarama.ConsumerGroup
	Producer      sarama.AsyncProducer
	r             Repository
	c             Cache
	Wg            sync.WaitGroup
}

func New(consumerGroup sarama.ConsumerGroup, producer sarama.AsyncProducer, r Repository, c Cache) *Server {
	return &Server{
		ConsumerGroup: consumerGroup,
		Producer:      producer,
		r:             r,
		c:             c,
	}
}

func (server *Server) RunConsumerGroup(ctx context.Context) {
	go func() {
		for err := range server.ConsumerGroup.Errors() {
			log.Printf("consumer error: %v", err)
		}
	}()

	server.Wg.Add(1)
	go func() {
		defer server.Wg.Done()
		for {
			err := server.ConsumerGroup.Consume(ctx, []string{"reset_orders"}, server)
			if err != nil {
				log.Printf("consume error: %v", err)
			}
			if ctx.Err() != nil {
				return
			}
		}
	}()
}

func (server *Server) RunProducer() {
	go func() {
		for err := range server.Producer.Errors() {
			log.Printf("producer error: %v", err)
		}
	}()
}
