package orders

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-3/internal/models"
)

type Repository interface {
	AddProduct(ctx context.Context, userId, id uint64) (err error)
	UpdateProduct(ctx context.Context, userId, id uint64, count uint32) (err error)
	DeleteProduct(ctx context.Context, userId, id uint64) (err error)
	GetCart(ctx context.Context, userId uint64) (products []models.Product, err error)
	DeleteCart(ctx context.Context, userId uint64) (err error)
	CreateOrder(ctx context.Context, userId uint64, products []models.Product) (id uint64, err error)
	UpdateStatus(ctx context.Context, id uint64, status string) (err error)
}
